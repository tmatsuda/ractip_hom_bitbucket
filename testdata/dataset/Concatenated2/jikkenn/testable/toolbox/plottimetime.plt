set xlabel "homs1-time"
set ylabel "homs4-time"
set datafile separator ","
set title "time1-time4plot-sameparameter-singledata"
plot "f1-f4time.csv" using 1:2
replot x
set term png
set output "time1-time4plot-sameparameter-singledata.png"
replot
set output
