# -*- coding: utf-8 -*-
require './sortingcsvlist.rb'

=begin
Must Run in "$RRTEST/toolbox"
=end



datafplist = []
datastrlist=`ls ./onlyhbp/*/tritest*num*4_wh*/total*fmeasure.csv`.split(" ")

for filename in datastrlist do
  puts "open! --- #{filename}"
  datafplist << open(filename,'r')
end

datafloatlist=[]
for fNfile in datafplist do
  fNfilelist = []
  fNfile.gets
  while line=fNfile.gets do
    fNcsvlist=line.split(",")
    fNfloatlist=[]
    for i in fNcsvlist do
      fNfloatlist << i.to_f
    end
     fNfilelist << fNfloatlist
  end
  datafloatlist << fNfilelist
  fNfile.close
end

sorted_datafloatlist = []
for fNfloatlist2 in datafloatlist do
  if not(fNfloatlist2.length == datafloatlist[0].length)
    puts "The data length is different from another one!!\n This may mean different parameter set was used."
  end
  sorted_datafloatlist << sortby3tops(fNfloatlist2)
end


averageFlist=Array.new(sorted_datafloatlist[0].length, 0.0)
for tmp in sorted_datafloatlist do
  for j in 0..(tmp.length-1) do
    averageFlist[j]=tmp[j][2]+averageFlist[j]
  end
end

output=open("averageFscorebydataforeachparameter_onlyhbp.csv", "w")
count  =0
for res in averageFlist do
  output.write("#{count},#{res}\n")
  count = count + 1
end
output.close
