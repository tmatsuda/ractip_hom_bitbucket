#!/bin/ruby

filepath=ARGV[0]
f = open(filepath)
result=""
firstlineflag = true
while line = gets
  if /^>/ =~ line
    if firstlineflag
      firstlineflag = false
      result << line
    elsif
    line = ("\n" << line)
    result << line
    next
    end
  else
    line = line.sub("\n", '')
    result << line
  end
end
f.close
out = open("ok-#{filepath}", 'w')
out.write(result)
out.close

