#!/bin/sh
#$ -S /bin/bash
#$ -cwd
#$ -v PATH
#$ -v LD_LIBRARY_PATH
#$ -e ./stderr.txt
#$ -o ./stdout.txt
#$ -l p=16
#$ -N RactIP_homologous
#$ -M mazda.takasky@gmail.com

${RACTIPHOM}/src/ractip_hom DIS.fa GenBank-FV536613.fa DIS.fa GenBank-FV536613.fa > result.fa
