#!/home/kakacy/lcl/bin/ruby
# -*- coding: utf-8 -*-
#$ -S /home/kakacy/lcl/bin/ruby
#$ -cwd
#$ -v PATH
#$ -v LD_LIBRARY_PATH
#$ -e ./stderr.txt
#$ -o ./stdout.txt
#$ -N RactIP_homologous
#$ -M mazda.takasky@gmail.com
#$ -t 1-2156

require 'benchmark'

`mkdir tritest_hyb_t_u_prev`

kparamlist = [0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,]
ijparamlist = Array(-4..9)
tasknumber = ENV["SGE_TASK_ID"].to_i
k = kparamlist[(tasknumber % 11)]
j = ijparamlist[(tasknumber / 11) % 14]
i = ijparamlist[(((tasknumber / 11) / 14) % 14)]

result = Benchmark.realtime do
`/home/kakacy/ractip-0.0.2/ractip -t #{1.0/((2**i).to_f+1.0)} -u #{1.0/((2**j).to_f+1.0)} -a #{k.to_f} MicA.fa ompA.fa > tritest_hyb_t_u_prev/ibp-#{(2**i).to_f}-hbp-#{(2**j).to_f}-alpha-#{k}.fa`
end
output = open("tritest_hyb_t_u_prev/ibp-#{(2**i).to_f}-hbp-s#{(2**j).to_f}-alpha-#{k}.fa", 'a')
output.write("time: #{result}\n")
output.close
