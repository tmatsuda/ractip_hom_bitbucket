#!/bin/ruby
# -*- coding: utf-8 -*-

=begin
Takashi Matsuda, 2014
MIT License	

Summing up values in csv files with a same parameter set.
=end
scoreary = []
n_hom=ARGV[0]
lsres=`ls U00096*/tritest_hyb_t_u_num_use_hom_#{n_hom}/total*.csv`
lslist=lsres.split(" ")

for i in lslist do
  scoreary << open(i.to_s)
end

`mkdir ./num_hom_#{n_hom}`
averaged_file=open("./num_hom_#{n_hom}/total-count-sen-ppv-fmeasure-averaged.csv", 'w')

tag = ""
for i in scoreary do
  tag = i.gets
end
averaged_file.write(tag)

while first = scoreary[0].gets do
  first_vals=first.split(',')
  follows=[]
  follows_vals=[]
  for i in scoreary[1..(scoreary.length-1)] do
    follows << i.gets
    follows_vals << follows[-1].split(',')
    if not(first_vals[0].to_f==follows_vals[-1][0].to_f)
      puts "ALERT:: incorrect parameter sets readed"
    end
  end
  averaged_file.write("#{first_vals[0]},#{first_vals[1]},#{first_vals[2]},")
  avg_sen = first_vals[3].to_f
  avg_ppv = first_vals[4].to_f
  avg_fmeasure = first_vals[5].to_f
  avg_time = first_vals[6].to_f
  for i in follows_vals do
    avg_sen = avg_sen + i[3].to_f
    avg_ppv = avg_ppv + i[4].to_f
    avg_fmeasure = avg_fmeasure + i[5].to_f
    avg_time = avg_time + i[6].to_f
  end
  avg_sen = avg_sen / (follows_vals.length+1).to_f
  avg_ppv = avg_ppv / (follows_vals.length+1).to_f
  avg_fmeasure = avg_fmeasure / (follows_vals.length+1).to_f
  avg_time = avg_time / (follows_vals.length+1).to_f
end
first.close
for i in follows do
  i.close
end
averaged_file.close
