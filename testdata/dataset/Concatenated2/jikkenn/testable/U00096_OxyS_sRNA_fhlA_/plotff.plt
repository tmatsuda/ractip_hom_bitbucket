set xlabel "homs1-fmeasure"
set ylabel "homs8-fmeasure"
set datafile separator ","
plot "f1-f8.csv" using 1:2
replot x
set term png
set output "f1-f8.png"
replot
set output
