# -*- coding: utf-8 -*-
require './sortingcsvlist.rb'


f1_val_list=[]
f8_val_list=[]
f1=open("tritest_hyb_t_u_num_use_hom_1/total-count-sen-ppv-fmeasure.csv")
f8=open("tritest_hyb_t_u_num_use_hom_#{ARGV[0]}/total-count-sen-ppv-fmeasure.csv")
output=open("f1-f#{ARGV[0]}.csv", "w+")
f1.gets
f8.gets
while ((f1_line=f1.gets) and (f8_line=f8.gets)) do
  f1csvlist=f1_line.split(",")
  f8csvlist=f8_line.split(",")
  f1floatlist = []
  f8floatlist = []
  for i in f1csvlist do
    f1floatlist << i.to_f
  end
  for j in f8csvlist do
    f8floatlist << j.to_f
  end
  f1_val_list << f1floatlist
  f8_val_list << f8floatlist
end

#sort
sorted_f1list=sortby3tops(f1_val_list)
sorted_fNlist=sortby3tops(f8_val_list)

if not(sorted_f1list.length == sorted_fNlist.length)
  puts "The data length is different from another one."
end

outputtime = open("f1-f#{ARGV[0]}time.csv", "w+")
for i in 1..(sorted_f1list.length-1) do
  output.write("#{sorted_f1list[i][2]},#{sorted_fNlist[i][2]}\n")
  outputtime.write("#{sorted_f1list[i][3]},#{sorted_fNlist[i][3]}\n")
end

f1.close
f8.close
output.close
outputtime.close

