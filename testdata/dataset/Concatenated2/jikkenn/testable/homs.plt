set xrange [0:1]
set yrange [0:1]
set xlabel "ppv"
set ylabel "sensitivity"
set datafile separator ","
plot "tritest_hyb_t_u_num_use_hom_1/total-count-sen-ppv-fmeasure.csv" using 5:4 ,"tritest_hyb_t_u_num_use_hom_2/total-count-sen-ppv-fmeasure.csv" using 5:4 ,"tritest_hyb_t_u_num_use_hom_4/total-count-sen-ppv-fmeasure.csv" using 5:4 ,"tritest_hyb_t_u_num_use_hom_8/total-count-sen-ppv-fmeasure.csv" using 5:4 ,"tritest_hyb_t_u_num_use_hom_16/total-count-sen-ppv-fmeasure.csv" using 5:4
set term png
set output "MicA-ompA-hom1.png"
replot
set output
