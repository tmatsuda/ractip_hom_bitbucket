#!/bin/sh
#$ -S /bin/bash
#$ -cwd
#$ -v PATH
#$ -v LD_LIBRARY_PATH
#$ -e ./stderr.txt
#$ -o ./stdout.txt
#$ -N RactIP_homologous
#$ -M mazda.takasky@gmail.com

#PARALLELLIMIT=10
#NUM_PROCESS=0
mkdir tritest_hyb_t_u
for i in 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 6 7 8 10 15 20
do
    for j in 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 6 7 8 10 15 20
    do
	for k in 0.01 0.02 0.04 0.05 0.075 0.1 0.133 0.166 0.2 0.233 0.266 0.3 0.35 0.4 0.45 0.5 0.6 0.7 0.8 0.9 1.0 2 3 5
	do
	    echo "ibp == ${i}"
	    echo "hbp == ${j}"
	    echo "alpha == ${k}"
	    qsub  qsubjobedtest.sh ${i} ${j} ${k}
	done
    done
done
