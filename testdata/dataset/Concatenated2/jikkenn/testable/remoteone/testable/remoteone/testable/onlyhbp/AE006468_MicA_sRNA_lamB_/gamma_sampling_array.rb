#!/home/kakacy/lcl/bin/ruby
# -*- coding: utf-8 -*-
#$ -S /home/kakacy/lcl/bin/ruby
#$ -cwd
#$ -v PATH
#$ -v LD_LIBRARY_PATH
#$ -e ./stderr.txt
#$ -o ./stdout.txt
#$ -N RactIP_homologous
#$ -M mazda.takasky@gmail.com
#$ -t 1-2156

require 'benchmark'
num_use_hom=ARGV[0]
`mkdir tritest_hyb_t_u_num_use_hom_#{num_use_hom}`

kparamlist = [0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
ijparamlist = Array(-4..9)
tasknumber = ENV["SGE_TASK_ID"].to_i
k = kparamlist[(tasknumber % 11)]
j = ijparamlist[(tasknumber / 11) % 14]
i = ijparamlist[(((tasknumber / 11) / 14) % 14)]
# puts `echo "ibp == #{(2**i).to_f}"`
# puts `echo "hbp == #{(2**j).to_f}"`
# puts `echo "alpha == #{k}"`

result = Benchmark.realtime do
`/home/kakacy/cbrc/ractip_hom/ractip_hom/src/ractip_hom -t #{(2**i).to_f} -u #{(2**j).to_f} -a #{k.to_f} --number-homologous=#{num_use_hom} MicA.fa ok_AE006468_MicA_sRNA_lamB_1.fa lamB.fa  ok_AE006468_MicA_sRNA_lamB_2.fa > tritest_hyb_t_u_num_use_hom_#{num_use_hom}/ibp-#{(2**i).to_f}-hbp-#{(2**j).to_f}-alpha-#{k}.fa`
end
output = open("tritest_hyb_t_u_num_use_hom_#{num_use_hom}/ibp-#{(2**i).to_f}-hbp-#{(2**j).to_f}-alpha-#{k}.fa", 'a')
output.write("time: #{result}\n")
output.close
