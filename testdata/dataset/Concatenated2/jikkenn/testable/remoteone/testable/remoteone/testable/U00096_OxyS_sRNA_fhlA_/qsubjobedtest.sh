#!/bin/sh
#$ -S /bin/bash
#$ -cwd
#$ -v PATH
#$ -v LD_LIBRARY_PATH
#$ -e ./stderr.txt
#$ -o ./stdout.txt
#$ -N RactIP_homologous
#$ -M mazda.takasky@gmail.com


${RACTIPHOM}/src/ractip_hom -a $3 -t $1 -u $2 OxyS.fa U00096_OxyS_sRNA_fhlA_ok_1.fa fhlA.fa U00096_OxyS_sRNA_fhlA_ok_2.fa > tritest_hyb_t_u/ibp-$1-hbp-$2-alpha-$3.fa
