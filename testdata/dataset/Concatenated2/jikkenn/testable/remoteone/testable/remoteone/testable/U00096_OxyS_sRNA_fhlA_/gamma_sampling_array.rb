#!/home/kakacy/lcl/bin/ruby
# -*- coding: utf-8 -*-
#$ -S /home/kakacy/lcl/bin/ruby
#$ -cwd
#$ -v PATH
#$ -v LD_LIBRARY_PATH
#$ -e ./stderr.txt
#$ -o ./stdout.txt
#$ -N RactIP_homologous
#$ -M mazda.takasky@gmail.com
#$ -t 1-324

require 'benchmark'
num_use_hom=8

kparamlist = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
ijparamlist = Array(0..5)
whparamlist = [0.0]
tasknumber = ENV["SGE_TASK_ID"].to_i
k = kparamlist[(tasknumber % 9)]
j = ijparamlist[(tasknumber / 9) % 6]
i = ijparamlist[(((tasknumber / 9) / 6) % 6)]
wh = whparamlist[((((tasknumber / 9) / 6) / 6) % 4)]
`mkdir tritest_hyb_t_u_num_use_hom_#{num_use_hom}_wh_#{wh}`

result = Benchmark.realtime do
`/home/kakacy/cbrc/ractip_hom/ractip_hom/src/ractip_hom -t #{(2**i).to_f} -u #{(2**j).to_f} -a #{k.to_f} --number-homologous=#{num_use_hom} --hyb-mix-w=#{wh} OxyS.fa U00096_OxyS_sRNA_fhlA_ok_1.fa fhlA.fa U00096_OxyS_sRNA_fhlA_ok_2.fa > tritest_hyb_t_u_num_use_hom_#{num_use_hom}_wh_#{wh}/ibp-#{(2**i).to_f}-hbp-#{(2**j).to_f}-alpha-#{k}-wh-#{wh}.fa`
end
output = open("tritest_hyb_t_u_num_use_hom_#{num_use_hom}_wh_#{wh}/ibp-#{(2**i).to_f}-hbp-#{(2**j).to_f}-alpha-#{k}-wh-#{wh}.fa", 'a')
output.write("time: #{result}\n")
output.close

