#!/home/kakacy/lcl/bin/ruby
# -*- coding: utf-8 -*-
#$ -S /home/kakacy/lcl/bin/ruby
#$ -cwd
#$ -v PATH
#$ -v LD_LIBRARY_PATH
#$ -e ./stderr.txt
#$ -o ./stdout.txt
#$ -N RactIP
#$ -M mazda.takasky@gmail.com
#$ -t 1-180

require 'benchmark'
num_use_hom=4

current=Dir::pwd
cdname=current.split('/')[-1]
target = Array.new(2)
target[0]="#{cdname.split('_')[1]}.fa"
target[1]="#{cdname.split('_')[3]}.fa"

filelist=Dir::entries(current)
filename = Array.new(2)
## ディレクトリ内に_ok*があったらそれを用い、なかったら用いない。
for fn in filelist do
  if fn =~ /U00*ok*/
    filename[0] = "#{cdname}ok_1.fa"
    filename[1] = "#{cdname}ok_2.fa"
  elsif fn =~ /ok*U00*/
    filename[0] = "ok_#{cdname}1.fa"
    filename[1] = "ok_#{cdname}2.fa"
  else
    filename[0]="#{cdname}1.fa"
    filename[1]="#{cdname}2.fa"
  end
end

# kparamlist = [0.1, 0.3, 0.5, 0.7, 0.9]
# ijparamlist = Array(0..5)
# #whparamlist = [0.0, 0.3, 0.5, 1.0]
# tasknumber = ENV["SGE_TASK_ID"].to_i
# k = kparamlist[(tasknumber % 5)]
# j = ijparamlist[(tasknumber / 5) % 6]
# i = ijparamlist[(((tasknumber / 5) / 6) % 6)]
# #wh = whparamlist[((((tasknumber / 9) / 6) / 6) % 4)]
`mkdir tritest_hyb_t_u_prev_default`

puts "#{target[0]} - #{target[1]}"

result = Benchmark.realtime do
`/home/kakacy/ractip-0.0.2/ractip #{target[0]} #{target[1]} > tritest_hyb_t_u_prev_default/result.fa`
end
output = open("tritest_hyb_t_u_prev_default/result.fa", 'a')
output.write("time: #{result}\n")
output.close
