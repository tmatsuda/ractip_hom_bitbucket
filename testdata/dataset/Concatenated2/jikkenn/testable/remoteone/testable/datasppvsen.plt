set xrange [0.2:1]
set yrange [0.2:1]
set xlabel "ppv"
set ylabel "sensitivity"
set datafile separator ","
set key left top
set title "suggested vs RactIP"
##plot "tritest_hyb_t_u_num_use_hom_1/total-count-sen-ppv-fmeasure.csv" using 5:4 ,"tritest_hyb_t_u_num_use_hom_2/total-count-sen-ppv-fmeasure.csv" using 5:4 ,"tritest_hyb_t_u_num_use_hom_4/total-count-sen-ppv-fmeasure.csv" using 5:4 ,"tritest_hyb_t_u_num_use_hom_8/total-count-sen-ppv-fmeasure.csv" using 5:4 ,"tritest_hyb_t_u_num_use_hom_16/total-count-sen-ppv-fmeasure.csv" using 5:4
plot "083207data2.csv" using 5:4 w lp lt 1 pt 1 ps 0.5 lc rgb "red" title "suggested method", "prevdata.csv" using 5:4 w lp lt 1 pt 6 ps 0.5 lc rgb "gray" title "RactIP"

set term pngcairo size 800,600
set output "323207datas.png"
replot
set output
