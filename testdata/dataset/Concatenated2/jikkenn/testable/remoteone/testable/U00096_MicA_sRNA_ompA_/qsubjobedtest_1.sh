#!/bin/sh
#$ -S /bin/bash
#$ -cwd
#$ -v PATH
#$ -v LD_LIBRARY_PATH
#$ -e ./stderr.txt
#$ -o ./stdout.txt
#$ -N RactIP_homologous
#$ -M mazda.takasky@gmail.com
#$ -t 1-1000


time ${RACTIPHOM}/src/ractip_hom -t $1 -u $2 -a $3 MicA.fa MicA_hom_top1_1.fa ompA.fa ompA_hom_top1.fa > tritest_hyb_t_u_1122/ibp-$1-hbp-$2-alpha-$3.fa
