#!/bin/sh
#$ -S /bin/bash
#$ -cwd
#$ -v PATH
#$ -v LD_LIBRARY_PATH
#$ -e ./stderr.txt
#$ -o ./stdout.txt
#$ -N RactIP_homologous
#$ -M mazda.takasky@gmail.com


${RACTIPHOM}/src/ractip_hom -a $1 SgrS.fa ok_U00096_SgrS_sRNA_ptsG_1.fa ptsG.fa ok_U00096_SgrS_sRNA_ptsG_2.fa > tritest_hyb_t_u/alpha-$1.fa
