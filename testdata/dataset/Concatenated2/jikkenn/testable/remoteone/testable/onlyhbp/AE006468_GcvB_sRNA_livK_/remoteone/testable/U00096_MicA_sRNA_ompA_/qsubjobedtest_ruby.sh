#!/bin/sh
#$ -S /bin/bash
#$ -cwd
#$ -v PATH
#$ -v LD_LIBRARY_PATH
#$ -e ./stderr.txt
#$ -o ./stdout.txt
#$ -N RactIP_homologous
#$ -M mazda.takasky@gmail.com


time ${RACTIPHOM}/src/ractip_hom -a 0.05 -t $1 -u $2 -a $3 MicA.fa U00096_MicA_sRNA_ompA_1.fa ompA.fa U00096_MicA_sRNA_ompA_2.fa > tritest_hyb_t_u_1122/ibp-$1-hbp-$2-alpha-$3.fa
