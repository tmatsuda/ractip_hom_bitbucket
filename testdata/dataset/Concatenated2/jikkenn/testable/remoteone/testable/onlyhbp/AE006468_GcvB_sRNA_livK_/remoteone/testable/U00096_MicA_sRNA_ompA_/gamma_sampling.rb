#!/bin/ruby

=begin
Takashi Matsuda, 2014
MIT License
=end

`mkdir tritest_hyb_t_u_1122`
kparamlist=[0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
for i in -4..9
  for j in -4..9
    for k in kparamlist
      puts `echo "ibp == #{(2**i).to_f}"`
      puts `echo "hbp == #{(2**j).to_f}"`
      puts `echo "alpha == #{k}"`
      puts `qsub qsubjobedtest_1.sh #{(2**i).to_f} #{(2**j).to_f} #{k}`
    end
  end
end
