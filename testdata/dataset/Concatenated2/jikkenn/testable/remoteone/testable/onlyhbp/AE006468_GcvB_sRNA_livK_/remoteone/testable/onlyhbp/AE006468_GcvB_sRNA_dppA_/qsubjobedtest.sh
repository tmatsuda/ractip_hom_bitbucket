#!/bin/sh
#$ -S /bin/bash
#$ -cwd
#$ -v PATH
#$ -v LD_LIBRARY_PATH
#$ -e ./stderr.txt
#$ -o ./stdout.txt
#$ -N RactIP_homologous
#$ -M mazda.takasky@gmail.com


${RACTIPHOM}/src/ractip_hom -a $1 GcvB.fa AE006468_GcvB_sRNA_dppA_ok_1.fa dppA.fa AE006468_GcvB_sRNA_dppA_ok_2.fa > tritest_hyb_t_u/alpha-$1.fa
