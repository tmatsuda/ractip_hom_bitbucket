#!/bin/sh
#$ -S /bin/bash
#$ -cwd
#$ -v PATH
#$ -v LD_LIBRARY_PATH
#$ -e ./stderr.txt
#$ -o ./stdout.txt
#$ -N RactIP_homologous
#$ -M mazda.takasky@gmail.com


/home/kakacy/ractip-0.0.2/ractip -t $1 -u $2 -a $3 MicA.fa ompA.fa > tritest_hyb_t_u/previous/ibp-$1-hbp-$2-alpha-$3.fa
