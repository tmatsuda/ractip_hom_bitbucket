#!/bin/ruby

=begin
Takashi Matsuda, 2014
MIT License
=end

`mkdir tritest_hyb_t_u`
`mkdir tritest_hyb_t_u/alpha_0.05`
for i in -5..10
  for j in -5..10
    puts `echo "ibp == #{(2**i).to_f}"`
    puts `echo "hbp == #{(2**j).to_f}"`
    puts `qsub qsubjobedtest_ruby.sh #{(2**i).to_f} #{(2**j).to_f}`
  end
end

