#!/bin/sh
#$ -S /bin/bash
#$ -cwd
#$ -v PATH
#$ -v LD_LIBRARY_PATH
#$ -e ./stderr.txt
#$ -o ./stdout.txt
#$ -N RactIP_homologous
#$ -M mazda.takasky@gmail.com


${RACTIPHOM}/src/ractip_hom -a $1 MicC.fa ok_U00096_MicC_sRNA_ompC_1.fa ompC.fa ok_U00096_MicC_sRNA_ompC_2.fa > tritest_hyb_t_u/alpha-$1.fa
