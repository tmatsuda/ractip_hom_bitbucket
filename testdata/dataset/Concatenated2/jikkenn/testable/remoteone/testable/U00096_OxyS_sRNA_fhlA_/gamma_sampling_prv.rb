#!/bin/ruby

=begin
Takashi Matsuda, 2014
MIT License
=end

`mkdir tritest_hyb_t_u`
`mkdir tritest_hyb_t_u/previous`
for i in [0.01, 0.05, 0.1, 0.2, 0.3, 0.4]
  for j in [0.01, 0.05, 0.1, 0.2, 0.3, 0.4]
    for k in [0.1, 0.3, 0.5, 0.7, 0.9]
      puts `echo "ibp == #{i.to_f}"`
      puts `echo "hbp == #{j.to_f}"`
      puts `echo "alpha == #{k}"`
      puts `qsub qsubjobedtest_prv.sh #{i.to_f} #{j.to_f} #{k.to_f}`
    end
  end
end

