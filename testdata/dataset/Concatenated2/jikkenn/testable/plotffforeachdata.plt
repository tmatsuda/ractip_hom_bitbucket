set xlabel "homs1-f"
set ylabel "homs4-f"
set xrange [0:1]
set yrange [0:1]
set datafile separator ","
set title "f1-f4plot-eachdata-gs16,gh4,alpha0.8"
plot "MicA-ompA-f1-f8-param16-4-08.csv" using 1:2
replot "OxyS-fhlA-f1-f8-param16-4-08.csv" using 1:2
replot "RyhB-sodB-f1-f8-param16-4-08.csv" using 1:2
replot x
set term png
set output "f1-f4plot-eachdata.png"
replot
set output