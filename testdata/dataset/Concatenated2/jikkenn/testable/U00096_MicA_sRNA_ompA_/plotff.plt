set xlabel "homs1-fmeasure"
set ylabel "homs4-fmeasure"
set xrange [0:1]
set yrange [0:1]
set datafile separator ","
set title "f1-f4plot-sameparameter-singledata"
plot "f1-f4.csv" using 1:2
replot x
set term png
set output "f1-f4plot-sameparameter-singledata.png"
replot
set output
